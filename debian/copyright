Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-sdk
Source: git://anongit.kde.org/plasma-sdk

Files: *
Copyright: 2006-2009, Aaron Seigo <aseigo@kde.org>
           2012, Antonis Tsiapaliokas <kok3rs@gmail.com>
           2010, Diego '[Po]lentino' Casella <polentino911@gmail.com>
           2009, Dmitry Suzdalev <dimsuz@gmail.com>
           1989-1991, Free Software Foundation, Inc
           2011-2014, Giorgos Tsiapaliokas <giorgos.tsiapaliokas@kde.org>
           2012-2013, Giorgos Tsiapaliokas <terietor@gmail.com>
           2013, Ivan Cukic <ivan.cukic@kde.org>
           2009-2010, Lim Yuen Hoe <yuenhoe@hotmail.com>
           2012-2013, Marco Martin <mart@kde.org>
           2009-2011, Martin Gräßlin <mgraesslin@kde.org>
           2009, Riccardo Iaconelli <riccardo@kde.org>
           2009, Richard Moore <rich@kde.org>
           2014-2015, Sebastian Kügler <sebas@kde.org>
           2009-2014, A Plasma fejlesztői
           2009, Aaron J. Seigo
           2009-2012, Foireann Fhorbartha Plasma
           2001-2011, Free Software Foundation, Inc
           2008-2009, K Desktop Environment
           2009-2014, Plasma-Entwicklerteam
           2009, Rosetta Contributors and Canonical Ltd 2009
           2010, Rosetta Contributors and Canonical Ltd 2010
           2007-2015, This_file_is_part_of_KDE
           2006, กลุ่มผู้พัฒนา KDE
License: GPL-2+

Files: debian/*
Copyright: 2015, Harald Sitter <sitter@kde.org>
License: GPL-2+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public
 License version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; see the file COPYING.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version
 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.
